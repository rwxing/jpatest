package com.zhu.jpatest.dao;

import org.springframework.data.repository.CrudRepository;

import com.zhu.jpatest.entity.User;

public interface UserDao extends CrudRepository<User, Long> {
}
